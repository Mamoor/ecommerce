<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderproductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('orderproducts')) {
            Schema::create('orderproducts', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->timestamps();
                $table->string('order_id');
                $table->string('product_id');
                $table->string('quantity');
            });
        }
    }
            

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orderproducts');
    }
}
