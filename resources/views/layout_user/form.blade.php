@extends('layout_admin.master')


@section('content')
    <div class="page-center">
        <div class="page-center-in">
            <div class="container-fluid">
                <form>
                
                <!-- COMPONENT END -->
                <!-- <div class="form-group row">
                    <button type="submit" class="btn btn-primary pull-right" disabled>Submit</button>
                    <button type="reset" class="btn btn-danger">Reset</button>
                </div> -->
                    <div class="form-group row">
                        <label class="col-sm-2 form-control-label">Name</label>
                        <div class="col-sm-10">
                            <p class="form-control-static"><input type="text" class="form-control" id="inputPassword" placeholder="Text"></p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 form-control-label">Description</label>
                        <div class="col-sm-10">
                            <p class="form-control-static"><input type="text" class="form-control" id="inputPassword" placeholder="Text"></p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 form-control-label">Category</label>
                        <select name="category" class="col-sm-10 custom-select">
                            <option selected>Select Category</option>
                            <option value="Men">Men</option>
                            <option value="Women">Women</option>
                            <option value="Accesories">Accesories</option>
                        </select>
                    </div>
                    <div class="form-group row">
                    <label class="col-sm-2 form-control-label">Quantity</label>
                        <div class="col-sm-10">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <button type="button" class="quantity-left-minus btn btn-danger btn-number"  data-type="minus" data-field="">
                                        <span class="glyphicon glyphicon-minus"></span>
                                    </button>
                                </span>
                                <input type="text" id="quantity" name="quantity" class="form-control input-number" value="1" min="1" max="100">
                                <span class="input-group-btn">
                                    <button type="button" class="quantity-right-plus btn btn-success btn-number" data-type="plus" data-field="">
                                        <span class="glyphicon glyphicon-plus"></span>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 form-control-label">Price</label>
                        <div class="col-sm-10">
                            <p class="form-control-static"><input type="text" class="form-control" id="inputPassword" placeholder="Text"></p>
                        </div>
                    </div>
                    <div class="form-group row">
                    <label class="col-sm-2 form-control-label">Image</label>
                        <div class="col-sm-10 input-group input-file" name="Fichier1">
                            <input type="text" class="form-control" placeholder='Choose a file...' />			
                            <span class="input-group-btn">
                                <button class="btn btn-default btn-choose" type="button">Choose</button>
                            </span>
                        </div>
	                </div>
                    <div class="form-group row">
                        <div class="col-md-auto">
                            <button type="submit" class="btn btn-primary center-block">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
                    
@endsection