<head>
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Favicon-->
	<link rel="shortcut icon" href="{{asset('img/fav.png')}}">
	<!-- Author Meta -->
	<meta name="author" content="CodePixar">
	<!-- Meta Description -->
	<meta name="description" content="">
	<!-- Meta Keyword -->
	<meta name="keywords" content="">
	<!-- meta character set -->
	<meta charset="UTF-8">
	<!-- Site Title -->
	<title>Amar Ponno</title>
	<!--
		CSS
		============================================= -->
	<link rel="stylesheet" href="{{ asset('assets/front/css/linearicons.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/front/css/font-awesome.min.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/front/css/themify-icons.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/front/css/bootstrap.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/front/css/owl.carousel.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/front/css/nice-select.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/front/css/nouislider.min.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/front/css/ion.rangeSlider.css') }}" />
	<link rel="stylesheet" href="{{ asset('assets/front/css/ion.rangeSlider.skinFlat.css') }}" />
	<link rel="stylesheet" href="{{ asset('assets/front/css/magnific-popup.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/front/css/main.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/front/custom/css/custom.css') }}">

	<script src="{{ asset('assets/front/js/vendor/jquery-2.2.4.min.js') }}"></script>
</head>