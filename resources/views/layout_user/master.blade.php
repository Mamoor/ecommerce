<!DOCTYPE html>
<html>
@include('layout_user.top')
<body>
@include('layout_user.header')
@yield('content')
@include('layout_user.footer')
@include('layout_user.foot')
</body>

</html>
