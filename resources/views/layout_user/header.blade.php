<!-- Start Header Area -->
<header class="header_area sticky-header">
		<div class="main_menu">
			<nav class="navbar navbar-expand-lg navbar-light main_box">
				<div class="container">
					<!-- Brand and toggle get grouped for better mobile display -->
					<a class="navbar-brand logo_h" href="{{url('/home/product')}}"><img src="{{asset('img/logo.png')}}" alt=""></a>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
					 aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse offset" id="navbarSupportedContent">
						<ul class="nav navbar-nav menu_nav ml-auto">
							<li class="nav-item"><a class="nav-link" href="{{url('home/product')}}">Home</a></li>
							
							<li class="nav-item"><a class="nav-link" href="{{url('/products/0')}}">shop</a></li>
							<li class="nav-item"><a class="nav-link" href="{{url('/contact')}}">Contact</a></li>
							@if(Auth::guard('usertable')->user()!= null)
							<li class="nav-item"><a class="nav-link" href="{{url('/get/profile/my-profile')}}">Profile</a></li>
							@endif
							<li class="nav-item">
								@if(Auth::guard('usertable')->user()== null)
								<button class="btn btn-rounded logout">
									<a class="logout-button" href="{{url('/login')}}">Log In</a>                    
								</button>
								@else
								<button class="btn btn-rounded logout">
									<a class="logout-button" href="{{url('/userlogout')}}">Log out</a>                    
								</button>
								@endif
							</li>
						</ul>
						<ul class="nav navbar-nav navbar-right">
							<li class="nav-item"><a href="{{url('/cart')}}" class="cart"><span class="ti-bag"></span></a></li>
						</ul>
					</div>
				</div>
			</nav>
		</div>
	</header>
	<!-- End Header Area -->