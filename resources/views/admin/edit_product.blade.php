@extends('layout_admin.admin_dash.master')

@section('content')
    <div class="page-content">
        <div class="page-center-in">
            <div class="container-fluid">
                <form class= "create" action="{{route('update.product',['id' => $Pltable->id ])}}" method="post" enctype="multipart/form-data">
                    <!-- COMPONENT END -->
                    <!-- <div class="form-group row">
                        <button type="submit" class="btn btn-primary pull-right" disabled>Submit</button>
                        <button type="reset" class="btn btn-danger">Reset</button>
                    </div> -->
                    <div class="form-group row">
                        <label class="col-sm-2 form-control-label">Name</label>
                        <div class="col-sm-10">
                            <p class="form-control-static"><input type="text" class="form-control" id="inputPassword" value="{{ $Pltable->name }}" name="name"></p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 form-control-label">Description</label>
                        <div class="col-sm-10">
                            <p class="form-control-static"><input type="text" class="form-control" id="inputPassword" value="{{ $Pltable->description}}" name="description"></p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 form-control-label">Category</label>
                        <select name="category" class="col-sm-10 custom-select">
                            <option selected>Select Category</option>
                            <option value="Men" @if($Pltable->category == "Men") selected @endif >Men</option>
                            <option value="Women" @if($Pltable->category == "Women") selected @endif >Women</option>
                            <option value="Accesories" @if($Pltable->category == "Accesories") selected @endif >Accesories</option>
                        </select>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 form-control-label">Quantity</label>
                        <div class="col-sm-10">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <button type="button" class="quantity-left-minus btn btn-danger btn-number"  data-type="minus" data-field="">
                                        <span class="glyphicon glyphicon-minus"></span>
                                    </button>
                                </span>
                                <input type="text" id="quantity" name="quantity" class="form-control input-number" value="{{ $Pltable->quantity}}" min="1" max="100">
                                <span class="input-group-btn">
                                    <button type="button" class="quantity-right-plus btn btn-success btn-number" data-type="plus" data-field="">
                                        <span class="glyphicon glyphicon-plus"></span>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 form-control-label">Price</label>
                        <div class="col-sm-10">
                            <p class="form-control-static"><input type="text" class="form-control" id="inputPassword" value="{{ $Pltable->price}}" name="price"></p>
                        </div>
                    </div>
                    <div class="form-group row">
                    <label class="col-sm-2 form-control-label">Image</label>
                        <div class="col-sm-10 input-group input-file" >
                            <div class="form-group">
                                <input type="file" id="BSbtninfo" name="image" value="{{ $Pltable->image}}">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-auto">
                            <button type="submit" class="btn btn-primary center-block">Update</button>
                        </div>
                    </div>
                        <input type="hidden" name="_token" value="{{Session::token()}}">
                </form>
            </div>
        </div>
    </div>
@endsection


   
                    