@extends('layout_admin.admin_dash.master')


@section('content')
    <div class="page-center">
        <div class="page-center-in">
            <div class="container-fluid">
                <form class="create" action="{{route('creat.category')}}" method="post">
                    <div class="form-group row">
                        <label class="col-sm-2 form-control-label">Name</label>
                        <div class="col-sm-10">
                            <p class="form-control-static"><input type="text" class="form-control" name="name" placeholder="Category Name"></p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <button type="submit" class="btn btn-primary pull-right">Submit</button>
                    </div>
                    <input type="hidden" name="_token" value="{{Session::token()}}">
                </form>
            </div>
        </div>
    </div>
                    
@endsection