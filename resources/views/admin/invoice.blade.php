@extends('layout_admin.admin_dash.master')

@section('content')
<div class="page-center">
    <div class="page-center-in">
        <div class="container-fluid">
            <div class="invoice-box">
                <table cellpadding="0" cellspacing="0">
                    <tr class="top">
                        <td colspan="2">
                            <table>
                                <tr>
                                    <td class="title">
                                        <img src="" style="width:100%; max-width:300px;">
                                    </td>
                                    <td></td>
                                    <td>
                                        Invoice #<br>
                                        Date: April 26, 2019<br>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    
                    <tr class="information">
                        <td colspan="2">
                            <table>
                                <tr class= "address">
                                    <td>
                                        Ponno.co<br>
                                        H#53, R#20<br>
                                        Nikunja-2, Dhaka.
                                    </td>
                                    <td></td>
                                    <td>
                                        
                                    name<br>
                                        phone <br>
                                        <address>Pallabi.</address>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    
                    <tr class="heading">
                        <td>
                            Item
                        </td>
                        <td>
                            Quantity
                        </td>
                        <td>
                            Price
                        </td>
                    </tr>

                    @foreach($orderproduct as $order)
                    <tr class="item">
                      <td>
                            {{$order->products_id->name}}
                        </td>
                        <td>
                           {{$order->quantity}}
                        </td>
                        <td>
                           {{$order->products_id->price}}
                        </td>
                    </tr>
                    @endforeach

                   <tr class="total">
                        <td></td>
                        <td></td>
                        <td>
                        Total:
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection