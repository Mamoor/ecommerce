@extends('layout_admin.admin_dash.master')


@section('content')
    <div class="page-center">
        <div class="page-center-in">
            <div class="container-fluid">
                <form class= "create" action="{{route('update.admin',['id'=> $admin->id ])}}" method="post">
                <!-- COMPONENT END -->
                    
                    <div class="form-group row">
                        <label class="col-sm-2 form-control-label">Name</label>
                        <div class="col-sm-10">
                            <p class="form-control-static"><input type="text" class="form-control" name="name" value="{{ $admin->name}}" placeholder="Text"></p>
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label class="col-sm-2 form-control-label">Email</label>
                        <div class="col-sm-10">
                            <p class="form-control-static"><input type="text" class="form-control" name="email" value="{{ $admin->email}}" placeholder="email"></p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <button type="submit" class="btn btn-primary pull-right">Submit</button>
                    </div>
                    <input type="hidden" name="_token" value="{{Session::token()}}">

                </form>
            </div>
        </div>
    </div>
                    
@endsection