@extends('layout_admin.admin_dash.master')

@push('css')
    <link rel="stylesheet" href="{{ asset('assets/admin/css/lib/datatables-net/datatables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin/css/separate/vendor/datatables-net.min.css') }}">
@endpush

@section('content')
<div class="page-content">
		<div class="container-fluid">
            <section class="box-typical">
                <header class="box-typical-header">
                    <div class="tbl-row">
                        <div class="tbl-cell tbl-cell-title">
                            <h3>Products</h3>
                        </div>
                        <div class="tbl-cell tbl-cell-action-bordered">
                            <a class="btn btn-primary" href="{{url('admin/product/p_form')}}" role="button"><i class="fa fa-plus"></i>Create</a>
                        </div>
                    </div>
                </header>
            </section>
			<section class="card">
				<div class="card-block">
                    <table id="dtBasicExample" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                            <th class="th-sm">Name
                            </th>
                            <th class="th-sm">Position
                            </th>
                            <th class="th-sm">Office
                            </th>
                            <th class="th-sm">Age
                            </th>
                            <th class="th-sm">Start date
                            </th>
                            <th class="th-sm">Salary
                            </th>
                            <th class="th-sm">Actions
                            </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                            <td>Tiger Nixon</td>
                            <td>System Architect</td>
                            <td>Edinburgh</td>
                            <td>61</td>
                            <td>2011/04/25</td>
                            <td>$320,800</td>
                            <td class="text-right">
                                <a href="#" class="btn btn-inline btn-secondary"><i class="fa fa-edit"></i></a>
                                <a href="#" class="btn btn-inline btn-danger"><i class="fa fa-trash"></i></a>
                            </td>
                            </tr>
                            <tr>
                            <td>Garrett Winters</td>
                            <td>Accountant</td>
                            <td>Tokyo</td>
                            <td>63</td>
                            <td>2011/07/25</td>
                            <td>$170,750</td>
                            <td class="text-right">
                                <a href="#" class="btn btn-inline btn-secondary"><i class="fa fa-edit"></i></a>
                                <a href="#" class="btn btn-inline btn-danger"><i class="fa fa-trash"></i></a>
                            </td>
                            </tr>
                            <tr>
                            <td>Ashton Cox</td>
                            <td>Junior Technical Author</td>
                            <td>San Francisco</td>
                            <td>66</td>
                            <td>2009/01/12</td>
                            <td>$86,000</td>
                            <td class="text-right">
                                <a href="#" class="btn btn-inline btn-secondary"><i class="fa fa-edit"></i></a>
                                <a href="#" class="btn btn-inline btn-danger"><i class="fa fa-trash"></i></a>
                            </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
			</section>
		</div><!--.container-fluid-->
	</div><!--.page-content-->
        
@endsection

@push('script')
    <!-- Datatable Script starts -->
    <script src="{{ asset('assets/admin/js/lib/datatables-net/datatables.min.js') }}"></script>
    
        <script>
                $(document).ready(function () {
                $('#dtBasicExample').DataTable();
                $('.dataTables_length').addClass('bs-select');
            });
        </script> <!-- for datatable -->    
@endpush