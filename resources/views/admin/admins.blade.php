@extends('layout_admin.admin_dash.master')
@push('css')
<link href="{{ asset('assets/admin/custom/css/jquery.mswitch.css') }}" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('assets/admin/css/lib/datatables-net/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/admin/css/separate/vendor/datatables-net.min.css') }}">
@endpush
@section('content')
    <div class="page-content">
		<div class="container-fluid">
            <section class="box-typical">
                <header class="box-typical-header">
                    <div class="tbl-row">
                        <div class="tbl-cell tbl-cell-title">
                            <h3>Admins</h3>
                        </div>
                        <div class="tbl-cell tbl-cell-action-bordered">
                            <a class="btn btn-primary" href="{{url('admin/admins/create_admin')}}" role="button"><i class="fa fa-plus"></i>Create</a></div>
                        </div>
                </header>
                <div class="box-typical-body">
                    <div class="table-responsive">
                        <table id ="example" class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Image</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                      
           
                            <tbody>
                            @foreach($data['tables'] as $value)
                                <tr>
                                    <td class="table-photo">
                                        <img src="{{ asset('assets/admin/img/photo-64-1.jpg') }}" alt="" data-toggle="tooltip" data-placement="bottom" title="Nicholas<br/>Barrett">
                                    </td>
                                    <td>
                                         {{$value->name}}
                                    </td>
                                    <td class="table-icon-cell">
                                       {{$value->email}}
                                    </td>
                                    <td>
                                        <input type="checkbox" id="switcher-{{ $value->id }}" data-id="{{ $value->id }}"  class="m_switch_check" value="{{ $value->status }}">  
                                    </td>
                                    <td class="text-right">
                                        <a href="{{url('/admin/admins/edit_admin',$value->id)}}" class="btn btn-inline btn-secondary"><i class="fa fa-edit"></i></a>
                                        <a href="{{route('admindelete',$value->id)}}" class="btn btn-inline btn-danger"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>  
                        </table>
                    </div>
                </div><!--.box-typical-body-->
            </section><!--.box-typical-->
        </div>
    </div>
@endsection
@push('script')
    <script src="{{ asset('assets/admin/custom/js/jquery.mswitch.js') }}"></script>
    <script>
        $(document).ready(function(){
            $(".m_switch_check:checkbox").mSwitch({
                onRender:function(elem){
                    if (elem.val() == 0){
                        $.mSwitch.turnOff(elem);
                    }else{
                        $.mSwitch.turnOn(elem);
                    }
                },
                onTurnOn: function(elem){
                    if (elem.val() == "0"){
                        elem.val("1");
                        var id = elem.data("id");
                        $.ajax({
                            url : "{{ route('admin.change.status') }}",
                            method: "POST",
                            data: { "_token" : "{{ csrf_token() }}", "id" : id, "status" : elem.val() },
                            success: function(response){
                                if(response.success){
                                    console.log(response.msg)
                                } else {
                                    console.log(response.msg);
                                }
                            }
                        })
                    }
                },

                onTurnOff: function(elem){
                    if (elem.val() == "1"){
                        elem.val("0");
                        var id = elem.data("id");
                        $.ajax({
                            url : "{{ route('admin.change.status') }}",
                            method: "POST",
                            data: { "_token" : "{{ csrf_token() }}", "id" : id, "status" : elem.val() },
                            success: function(response){
                                if(response.success){
                                    console.log(response.msg)
                                } else {
                                    console.log(response.msg);
                                }
                            }
                        })
                    }
                },

            });
        });
    </script>

    <!-- Datatable Script starts -->
    <script src="{{ asset('assets/admin/js/lib/datatables-net/datatables.min.js') }}"></script>
    
        <script>
            $(document).ready(function () {
                $('#example').DataTable();
                $('.dataTables_length').addClass('bs-select');
            });
    </script> <!-- for datatable -->    
@endpush