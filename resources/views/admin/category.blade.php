@extends('layout_admin.admin_dash.master')

@push('css')
    <link rel="stylesheet" href="{{ asset('assets/admin/css/lib/datatables-net/datatables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin/css/separate/vendor/datatables-net.min.css') }}">
@endpush

@section('content')
    <div class="page-content">
		<div class="container-fluid">
            <section class="box-typical">
                <header class="box-typical-header">
                    <div class="tbl-row">
                        <div class="tbl-cell tbl-cell-title">
                            <h3>Category</h3>
                        </div>
                        <div class="tbl-cell tbl-cell-action-bordered">
                            <a class="btn btn-primary" href="{{url('/admin/category/create_category')}}" role="button"><i class="fa fa-plus"></i>Create</a>
                        </div>
                    </div>
                </header>
                <div class="box-typical-body">
                    <div class="table-responsive">
                        <table id="example" class="table table-hover">
                            <thead>
                                <tr>
                                   <th>Category name</th>
                                    <th>Category ID</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($data['tables'] as $singlevalue)
                                <tr>
                                    <!-- <td class="table-photo">
                                     <img src="{{ asset('storage/'.$singlevalue->image)}}" alt="" data-toggle="tooltip" data-placement="bottom" title="Nicholas<br/>Barrett">
                                    </td> -->
                                    <td>
                                        {{$singlevalue->name}}

                                    </td>
                                    <td class="table-icon-cell">
                                        {{$singlevalue->id}}
                                    </td>
                                    <td class="text-right">
                                        <a href="{{url('/admin/category/edit_category',$singlevalue->id)}}" class="btn btn-inline btn-secondary"><i class="fa fa-edit"></i></a>
                                        <a href="{{route('category.delete',$singlevalue->id)}}" class="btn btn-inline btn-danger"><i class="fa fa-trash"><span>Delete</span></i></a>
                                    </td>
                                </tr>
                             @endforeach
                            </tbody>
                        </table>
                    </div>
                </div><!--.box-typical-body-->
            </section><!--.box-typical-->
        </div>
    </div>
@endsection

@push('script')
    <!-- Datatable Script starts -->
    <script src="{{ asset('assets/admin/js/lib/datatables-net/datatables.min.js') }}"></script>
    
        <script>
            $(document).ready(function () {
                $('#example').DataTable();
                $('.dataTables_length').addClass('bs-select');
            });
        </script> <!-- for datatable -->    
@endpush