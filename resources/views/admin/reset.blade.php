@extends('layout_admin.master')

@section('content')
    <div class="page-center">
        <div class="page-center-in">
            <div class="container-fluid">
                <form class="sign-box reset-password-box" action="{{route('resetController')}}" method="post">
                    <!--<div class="sign-avatar">
                        <img src="img/avatar-sign.png" alt="">
                    </div>-->
                    <header class="sign-title">Reset Password</header>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="E-Mail or Phone" name="reset_password" />
                    </div>
                    <button type="submit" class="btn btn-rounded">Reset</button>
                    or <a href="{{url('/signin')}}">Sign in</a>
                </form>
            </div>
        </div>
    </div><!--.page-center-->
@endsection