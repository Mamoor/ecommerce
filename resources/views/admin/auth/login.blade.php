@extends('layout_admin.master')

@section('content')
    @if ($errors->any())
            {{ implode('', $errors->all('<div>:message</div>')) }}
    @endif
    <div class="page-center">
        <div class="page-center-in">
            <div class="container-fluid">
                <form class="sign-box" action="{{route('admin-login')}}" method="post">
                    <div class="sign-avatar">
                        <img src="{{ asset('assets/admin/img/avatar-sign.png') }}" alt="">
                    </div>
                    <header class="sign-title">Sign In</header>
                      <div class="form-group">
                        <input type="email" class="form-control" placeholder="E-Mail" name="email" />
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" placeholder="Password" name="password" />
                    </div>
                    <button type="submit" class="btn btn-rounded">Sign in</button>
                    <div class="form-group"> 
                     <div class="float-right reset">
                        <a href="{{url('/reset')}}">Reset password</a>  
                     </div>
                    </div>
                    <p class="sign-note">New to our website? <a href="{{url('/signup')}}">Sign up</a></p>
                    <!--<button type="button" class="close">
                        <span aria-hidden="true">&times;</span>
                    </button>-->
                 <input type="hidden" name="_token" value="{{Session::token()}}">
                </form>
            </div>
        </div>
    </div><!--.page-center-->

@endsection