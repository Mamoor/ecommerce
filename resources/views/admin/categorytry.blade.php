@extends('layout_admin.admin_dash.master')

@section('content')
    <div class="mobile-menu-left-overlay"></div>
    <nav class="side-menu">
        <ul class="side-menu-list">
            <li class="grey with-sub">
                <a href="#">
                    <span>
                    <i class="font-icon font-icon-dashboard"></i>
                    <span class="lbl">Dashboard</span>
                    </span>
                </a>
                <!-- <ul>
                    <li><a href="{{url('/admin')}}"><span class="lbl">Default</span></a></li>
                    <li><a href="dashboard-top-menu.html"><span class="lbl">Products</span></a></li>
                    <li><a href="side-menu-compact-full.html"><span class="lbl">Compact menu</span></a></li>
                    <li><a href="dashboard-addl-menu.html"><span class="lbl">Submenu</span></a></li>
                    <li><a href="side-menu-avatar.html"><span class="lbl">Menu with avatar</span></a></li>
                    <li><a href="side-menu-avatar.html"><span class="lbl">Compact menu with avatar</span></a></li>
                    <li><a href="dark-menu.html"><span class="lbl">Dark menu</span></a></li>
                    <li><a href="dark-menu-blue.html"><span class="lbl">Blue dark menu</span></a></li>
                    <li><a href="dark-menu-green.html"><span class="lbl">Green dark menu</span></a></li>
                    <li><a href="dark-menu-green-compact.html"><span class="lbl">Green compact dark menu</span></a></li>
                    <li><a href="dark-menu-ultramarine.html"><span class="lbl">Ultramarine dark menu</span></a></li>
                    <li><a href="asphalt-menu.html"><span class="lbl">Asphalt top menu</span></a></li>
                    <li><a href="side-menu-big-icon.html"><span class="lbl">Big menu</span></a></li>
                </ul> -->
            </li>

            <li class="red">
                <a href="{{url('/admin/product')}}">
                    <i class="font-icon glyphicon glyphicon-send"></i>
                    <span class="lbl">Products</span>
                </a>
            </li>
            <li class="blue-dirty">
                <a href="{{url('/admin/category')}}">
                    <span class="glyphicon glyphicon-th"></span>
                    <span class="lbl">Category</span>
                </a>
            </li>
        </ul>
    </nav>

    <div class="page-content">
		<div class="container-fluid">
			<section class="box-typical">
				<div id="toolbar">
					<div class="bootstrap-table-header">Table header</div>
					<button id="remove" class="btn btn-danger remove" disabled>
						<i class="font-icon font-icon-close-2"></i> Delete
					</button>
				</div>
				<div class="table-responsive">
					<table id="table"
						   class="table table-striped"
						   data-toolbar="#toolbar"
						   data-search="true"
						   data-show-refresh="true"
						   data-show-toggle="true"
						   data-show-columns="true"
						   data-show-export="true"
						   data-detail-view="true"
						   data-detail-formatter="detailFormatter"
						   data-minimum-count-columns="2"
						   data-show-pagination-switch="true"
						   data-pagination="true"
						   data-id-field="id"
						   data-page-list="[10, 25, 50, 100, ALL]"
						   data-show-footer="false"
						   data-response-handler="responseHandler">
					</table>
				</div>
			</section><!--.box-typical-->

		</div><!--.container-fluid-->
	</div><!--.page-content-->
@endsection