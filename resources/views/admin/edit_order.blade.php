@extends('layout_admin.admin_dash.master')


@section('content')
    <div class="page-center">
        <div class="page-center-in">
            <div class="container-fluid">
                <form class= "create" action="#" method="post">
                <!-- COMPONENT END -->
                    
                    <div class="form-group row">
                        <label class="col-sm-2 form-control-label">Product name</label>
                        <div class="col-sm-10">
                            <p class="form-control-static"><input type="text" class="form-control" placeholder="Text"></p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 form-control-label">Address</label>
                        <div class="col-sm-10">
                            <p class="form-control-static"><input type="text" class="form-control" placeholder="Text"></p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 form-control-label">Phone</label>
                        <div class="col-sm-10">
                            <p class="form-control-static"><input type="text" class="form-control" placeholder="Text"></p>
                        </div>
                    </div>
                    
                    <!-- <div class="form-group row">
                        <label class="col-sm-2 form-control-label">Date</label>
                        <div class="col-sm-10">
                            <p class="form-control-static"><input type="text" class="form-control" id="inputPassword" placeholder="Text"></p>
                        </div>
                    </div> -->
                    <div class="form-group row">
                        <button type="submit" class="btn btn-primary pull-right">Submit</button>
                    </div>
                    <input type="hidden" name="_token" value="{{Session::token()}}">
                </form>
            </div>
        </div>
    </div>
                    
@endsection