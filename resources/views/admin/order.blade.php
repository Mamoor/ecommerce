@extends('layout_admin.admin_dash.master')

@push('css')
    <link rel="stylesheet" href="{{ asset('assets/admin/css/lib/datatables-net/datatables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin/css/separate/vendor/datatables-net.min.css') }}">
@endpush

@section('content')
    <div class="page-content">
		<div class="container-fluid">
            <section class="box-typical">
                <header class="box-typical-header">
                    <div class="tbl-row">
                        <div class="tbl-cell tbl-cell-title">
                            <h2>Orders</h2>
                        </div>
                    </div>
                </header>
                <div class="box-typical-body">
                    <div class="table-responsive">
                        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <th>client id</th>
                                <th>Name</th>
                                <th>Total tk</th>
                               <th>Address</th>
                                <th>Phone</th>
                               <th>Date</th>
                               <th>Invoice</th>
                                <th>Actions</th>
                            </thead>
                            <tbody>
                                 @foreach($data['tables'] as $vlaue)
                                <tr>
                                    <td>{{$vlaue->id}}</td>
                                    <td>{{$vlaue->order_of_user->name}}</td>
                                    <td>{{$vlaue->total}}</td>
                                    <td>{{$vlaue->order_of_user->address}}</td>
                                    <td>{{$vlaue->order_of_user->phonenumber}}</td>
                                   <td>{{$vlaue->created_at}}</td>
                                    <td class="invoice">
                                        <a href="{{url('/admin/invoice',$vlaue->id)}}" class= "btn btn-dark">Invoice</a>
                                    </td>
                                    <td class="text-right">
                                        <a href="{{url('/admin/order/edit_order',$vlaue->id)}}" class="btn btn-inline btn-secondary"><i class="fa fa-edit"></i></a>
                                        <a href="{{route('orderdelete',$vlaue->id)}}" class="btn btn-inline btn-danger"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                             @endforeach
                            </tbody>
                        </table>
                    </div>
                </div><!--.box-typical-body-->
            </section>
        </div>
    </div>
@endsection
@push('script')
    <!-- Datatable Script starts -->
    <script src="{{ asset('assets/admin/js/lib/datatables-net/datatables.min.js') }}"></script>
    
        <script>
            $(document).ready(function () {
                $('#example').DataTable();
                $('.dataTables_length').addClass('bs-select');
            });
        </script> <!-- for datatable -->    
@endpush