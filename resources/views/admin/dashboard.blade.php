@extends('layout_admin.admin_dash.master')

@section('content')
    <div class="page-content">
        <div class="container-fluid">
            <div class="row"> 
                <div class="col-xl-12">
                    <div class="row">
                        <div class="col-sm-3">
                
                            <article class="statistic-box red">
                                <div>
                                    <div class="number">{{$count}}</div>
                                    <div class="caption"><div>Total Product</div></div>
                                </div>
                            </article>
                        </div>
                        <!-- <div class="col-sm-3">
                            <article class="statistic-box purple">
                                <div>
                                    <div class="number">12</div>
                                    <div class="caption"><div>Total Category</div></div>
                                </div>
                            </article>
                        </div> -->
                        <div class="col-sm-3">
                            <article class="statistic-box yellow">
                                <div>
                                    <div class="number">{{$order}}</div>
                                    <div class="caption"><div>Total Order</div></div>
                                </div>
                            </article>
                        </div>
                        <div class="col-sm-3">
                            <article class="statistic-box green">
                                <div>
                                    <div class="number">{{$users}}</div>
                                    <div class="caption"><div>Total User</div></div>
                                </div>
                            
                            </article>
                        </div><!--.col-->
                    </div><!--.row-->
                </div><!--.col-->
            </div><!--.row-->
        </div>
    </div>
@endsection
