@extends('layout_admin.admin_dash.master')

@push('css')
    <link rel="stylesheet" href="{{ asset('assets/admin/css/lib/datatables-net/datatables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin/css/separate/vendor/datatables-net.min.css') }}">
@endpush

@section('content')
<div class="page-content">
		<div class="container-fluid">
            <section class="box-typical">
                <header class="box-typical-header">
                    <div class="tbl-row">
                        <div class="tbl-cell tbl-cell-title">
                            <h3>Products</h3>
                        </div>
                        <div class="tbl-cell tbl-cell-action-bordered">
                            <a class="btn btn-primary" href="{{url('admin/product/p_form')}}" role="button"><i class="fa fa-plus"></i>Create</a>
                        </div>
                    </div>
                </header>
            </section>
			<section class="card">
				<div class="card-block">
                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                            <th class="th-sm">
                                Image
                            </th>
                            <th class="th-sm">
                                Name
                            </th>
                            <th class="th-sm">
                                Category name
                            </th>
                            <th class="th-sm">
                                Quantity
                            </th>
                            <th class="th-sm">
                                Price
                            </th>
                            <th class="th-sm">
                                Create Time
                            </th>
                            <th class="th-sm">
                                Actions
                            </th>
                            </tr>
                        </thead>
                        <tbody>
						@foreach($data['tables'] as $singlevalue)
                            <tr>
                                <td class="table-photo">
                                    <img src="{{ asset('storage/'.$singlevalue->image)}}" alt="" data-toggle="tooltip" data-placement="bottom" title="Nicholas<br/>Barrett">
                                </td>
                                <td>
                                    {{$singlevalue->name}}
                                </td>
                                <td class="color-blue-grey-lighter"> 
                                    {{$singlevalue->categoryId->name}}
                                </td>
                                <td class="table-icon-cell">
                                    {{$singlevalue->quantity}}
                                </td>
                                <td class="table-icon-cell">
                                    {{$singlevalue->price}}
                                </td>
                                <td class="table-date">
                                    {{$singlevalue->updated_at}}
                                    <i class="font-icon font-icon-clock"></i>
                                </td>
                                <td class="text-right">
                                    <a href="{{route('productedit',$singlevalue->id)}}" class="btn btn-inline btn-secondary"><i class="fa fa-edit"></i></a>
                                    <a href="{{route('getdelete',$singlevalue->id)}}" class="btn btn-inline btn-danger"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
					</table>
                </div>
			</section>
		</div><!--.container-fluid-->
	</div><!--.page-content-->   
@endsection

@push('script')
    <!-- Datatable Script starts -->
    <script src="{{ asset('assets/admin/js/lib/datatables-net/datatables.min.js') }}"></script>
    
        <script>
            $(document).ready(function () {
                $('#example').DataTable();
                $('.dataTables_length').addClass('bs-select');
            });
        </script> <!-- for datatable -->    
@endpush