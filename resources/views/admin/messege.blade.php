@extends('layout_admin.admin_dash.master')

@push('css')
    <link rel="stylesheet" href="{{ asset('assets/admin/css/lib/datatables-net/datatables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin/css/separate/vendor/datatables-net.min.css') }}">
@endpush

@section('content')
    <div class="page-content">
		<div class="container-fluid">
            <section class="box-typical">
                <header class="box-typical-header">
                    <div class="tbl-row">
                        <div class="tbl-cell tbl-cell-title">
                            <h3>Messege</h3>
                        </div>
                        <!-- <div class="tbl-cell tbl-cell-action-bordered">
                            <a class="btn btn-primary" href="{{url('/admin/category/create_category')}}" role="button"><i class="fa fa-plus"></i>Create</a>
                        </div> -->
                    </div>
                </header>
                <div class="box-typical-body">
                    <div class="table-responsive">
                        <table id="example" class="table table-hover">
                            <thead>
                                <tr>
                                   <th>User Name</th>
                                    <th>Subject</th>
                                    <th>Date/Time</th>
                                    <th>Options</th>
                                </tr>
                            </thead>
                            @foreach($threads as $thread)
                            <tbody>
                                <tr>
                                    <td>
                                        {{$thread->thread->name}}
                                    </td>
                                    <td>
                                        {{$thread->subject}}
                                    </td>
                                    <td>
                                    {{$thread->created_at}}
                                    </td>
                                    <td>
                                        <a href="{{url('/admin/support/view/user/message')}}" class="btn btn-inline btn-secondary"><i class="fa fa-edit"></i>View Messege</a>
                                        <a href="{{url('/admin/support/delete/message',$thread->id)}}" class="btn btn-inline btn-danger"><i class="fa fa-trash"><span>Delete</span></i></a>
                                    </td>
                                </tr>
                            </tbody>
                            @endforeach
                        </table>
                    </div>
                </div><!--.box-typical-body-->
            </section><!--.box-typical-->
        </div>
    </div>
@endsection

@push('script')
    <!-- Datatable Script starts -->
    <script src="{{ asset('assets/admin/js/lib/datatables-net/datatables.min.js') }}"></script>
    
        <script>
            $(document).ready(function () {
                $('#example').DataTable();
                $('.dataTables_length').addClass('bs-select');
            });
        </script> <!-- for datatable -->    
@endpush