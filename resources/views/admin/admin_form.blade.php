@extends('layout_admin.admin_dash.master')


@section('content')
    <div class="page-center">
        <div class="page-center-in">
            <div class="container-fluid">
                <form class="create" action="" method="post">
                <!-- COMPONENT END -->
                    
                    <div class="form-group row">
                        <label class="col-sm-2 form-control-label">Name</label>
                        <div class="col-sm-10">
                            <p class="form-control-static"><input type="text" class="form-control" id="inputPassword" placeholder="Text"></p>
                        </div>
                    </div>
                    <div class="form-group row">
                    <label class="col-sm-2 form-control-label">Image</label>
                        <div class="col-sm-10 input-group input-file" name="Fichier1">
                            <input type="text" class="form-control" placeholder="Choose a file..." name="image" />          
                            <span class="input-group-btn">
                                <button class="btn btn-default btn-choose" type="button">Choose</button>
                            </span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 form-control-label">Email</label>
                        <div class="col-sm-10">
                            <p class="form-control-static"><input type="text" class="form-control" id="inputPassword" placeholder="Text"></p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 form-control-label">Role</label>
                        <div class="col-sm-10">
                            <p class="form-control-static"><input type="text" class="form-control" id="inputPassword" placeholder="Text"></p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <button type="submit" class="btn btn-primary pull-right">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
                    
@endsection