<div class="mobile-menu-left-overlay"></div>
        <nav class="side-menu">
            <ul class="side-menu-list">
                <li class="grey with-sub">
                    <a href="{{url('/admin/dashboard')}}">
                        <span>
                        <i class="font-icon font-icon-dashboard"></i>
                        <span class="lbl">Dashboard</span>
                        </span>
                    </a>
                </li>

                <li class="red">
                    <a href="{{url('/admin/product')}}">
                        <i class="font-icon glyphicon glyphicon-send"></i>
                        <span class="lbl">Products</span>
                    </a>
	            </li>
                <li class="blue-dirty">
                    <a href="{{url('/admin/category')}}">
                        <span class="glyphicon glyphicon-th"></span>
                        <span class="lbl">category</span>
                    </a>
	            </li>
                <li class="pink-red">
                    <a href="{{url('/admin/order')}}">
                        <i class="font-icon font-icon-zigzag"></i>
                        <span class="lbl">Order</span>
                    </a>
	            </li>
                <li class="gold with-sub">
                    <a href="{{url('/admin/admins')}}">
                        <span>
                            <i class="font-icon font-icon-edit"></i>
                            <span class="lbl">Admins</span>
                        </span>
                    </a>	            
                </li>
                <li class="gold with-sub">
                    <a href="{{url('/admin/support/view')}}">
                        <span>
                            <i class="font-icon font-icon-edit"></i>
                            <span class="lbl">Messseges</span>
                        </span>
                    </a>	            
	            </li>
            </ul>
        </nav>