<header class="site-header">
	<div class="container-fluid">
		<a href="#" class="site-logo">
			<img class="hidden-md-down" src="{{ asset('assets/admin/img/logo-2.png') }}" alt="">
		</a>
		<div class="site-header-content">
			<div class="site-header-content-in">
				<div class="site-header-shown">
						<div class="dropdown user-menu">
							<button class="dropdown-toggle" id="dd-user-menu" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style= "back">
								Ashik
							</button>
							<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dd-user-menu">
								<div class="dropdown-divider"></div>
									<a class="dropdown-item">
										<form action="{{ route('logout')}}" method="POST">
										<button type="submit"> 
												<span class="font-icon glyphicon glyphicon-log-out"></span>Logout
											</button>
											<input type="hidden" name="_token" value="{{Session::token()}}">
										</form>
									</a>
								</div>
							</div>
	
						<button type="button" class="burger-right">
							<i class="font-icon-menu-addl"></i>
						</button>
					</div><!--.site-header-shown-->
				</div><!--site-header-content-in-->
		</div><!--.site-header-content-->
	</div><!--.container-fluid-->
</header><!--.site-header-->