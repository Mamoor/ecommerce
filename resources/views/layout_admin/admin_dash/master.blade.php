<!DOCTYPE html>
<html>
@include('layout_admin.admin_dash.top')
<body class="with-side-menu control-panel control-panel-compact">
@include('layout_admin.admin_dash.header')
@include('layout_admin.admin_dash.navigation')
@yield('content')
@include('layout_admin.admin_dash.foot')
</body>
</html>