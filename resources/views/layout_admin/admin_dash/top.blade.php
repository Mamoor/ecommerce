<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>AMAR PONNO</title>

	<link href="{{ asset('assets/admin/img/favicon.144x144.png') }}" rel="apple-touch-icon" type="image/png" sizes="144x144">
	<link href="{{ asset('assets/admin/img/favicon.114x114.png') }}" rel="apple-touch-icon" type="image/png" sizes="114x114">
	<link href="{{ asset('assets/admin/img/favicon.72x72.png') }}" rel="apple-touch-icon" type="image/png" sizes="72x72">
	<link href="{{ asset('assets/admin/img/favicon.57x57.png') }}" rel="apple-touch-icon" type="image/png">
	<link href="{{ asset('assets/admin/img/favicon.png') }}" rel="icon" type="image/png">
	<link href="{{ asset('assets/admin/img/favicon.ico') }}" rel="shortcut icon">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
    <link rel="stylesheet" href="{{ asset('assets/admin/css/lib/lobipanel/lobipanel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin/css/separate/vendor/lobipanel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin/css/lib/jqueryui/jquery-ui.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin/css/separate/pages/widgets.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin/css/lib/font-awesome/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin/css/lib/bootstrap/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/admin/css/main.css') }}">
	@stack('css')
	<link rel="stylesheet" href="{{ asset('assets/admin/custom/css/custom.css') }}">
	

	<script src="{{ asset('assets/admin/js/lib/jquery/jquery-3.2.1.min.js') }}"></script>
</head>