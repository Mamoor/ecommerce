

<script type="text/javascript" src="{{ asset('assets/admin/js/lib/jqueryui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/admin/js/lib/popper/popper.min.js') }}"></script>
<script src="{{ asset('assets/admin/js/lib/tether/tether.min.js') }}"></script>
<script src="{{ asset('assets/admin/js/lib/bootstrap/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/admin/js/plugins.js') }}"></script>
@stack('script')