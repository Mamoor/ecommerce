<div class="container">
    <div class="messaging">
        <div class="inbox_msg">
            <div class="mesgs">
                <div class="msg_history">
                    @foreach($data['thread']->messages as $msg)
                    @if($msg->by_whom_type == 'admin')
                    <div class="incoming_msg">
                        <div class="received_msg">
                            <div class="received_withd_msg">
                                <p>{{$msg->message}}</p>
                                <span class="time_date"> {{$msg->created_at}}</span>
                            </div>
                        </div>
                    </div>
                    @else
                    <div class="outgoing_msg">
                        <div class="sent_msg">
                            <p>{{$msg->message}}</p>
                            <span class="time_date"> {{$msg->created_at}}</span>
                        </div>
                    </div>
                    @endif
                    @endforeach
                </div>
                <div class="type_msg">
                    <div class="input_msg_write">
                        <input type="text" class="write_msg" placeholder="Type a message" />
                        <button class="msg_send_btn" type="button"><i class="fa fa-paper-plane-o"
                                aria-hidden="true"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
