<section class="lattest-product-area pb-40 category-list">
	<div class="row">
		<!-- single product -->
		@foreach($products as $product)
		<div class="col-lg-4 col-md-6">
			<div class="single-product">
				<img class="img-fluid" src="{{ asset('storage/'.$product->image)}}" alt="" style= "height : 300px;">
				<div class="product-details">
					<a href="{{route('view.product',$product->id)}}">{{$product->name}}</a>
					<div class="price">
						<h6>{{$product->price}} tk</h6><br>
						<h6>Category:{{$product->categoryId->name}}</h6>
					</div>
					<div class="prd-bottom">
						<a href="{{route('cart.product',$product->id)}}" class="social-info @if(in_array($product->id, $product_selected)){{'not-active'}}@endif" >
							<span class="ti-bag"></span>
							<p class="hover-text">add to bag</p>
						</a>
					</div>
				</div>
			</div>
		</div>
		@endforeach
	</div>
</section>
<!-- End Best Seller -->
<!-- Start Filter Bar -->
<div class="filter-bar d-flex flex-wrap align-items-center">
	<div class="pagination">
		{{$products->links()}}
	</div>
</div>

