<div class="header-profile">
    <h2>My Profile</h2>
</div>
<div class="row">
    <div class="col-md-12">
        <table style="width:100%; max-width:100%">
            <tbody>
                <tr>
                    <td><b>Name</b></td>
                    <td align="left">{{$data['user']->name}}</td>
                </tr>
                <tr>
                    <td><b>Email</b></td>
                    <td>{{$data['user']->email}}</td>
                <tr>
                    <td><b>Contact No</b></td>
                    <td>{{$data['user']->phonenumber}}</td>
                </tr>
                <tr>
                    <td><b>Address</b></td>
                    <td>{{$data['user']->address}}</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
