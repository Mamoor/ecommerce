
<div class="header-profile">
    <h2>SUPPORT</h2>
</div>

<div id="exTab2" class="navtab">
    <div class="">
        <div class="col-xs-12 ">
            <nav>
                <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home"
                        role="tab" aria-controls="nav-home" aria-selected="true">messeges</a>
                    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile"
                        role="tab" aria-controls="nav-profile" aria-selected="false">create new</a>
                </div>
            </nav>
            <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                    <div class="subject">
                        <table class="view">
                            <tr class="head">
                                <th>Subject</th>
                                <th class="text-center">Option</th>
                            </tr>
                            @foreach ($data['threads'] as $item)
                            <tr class="table-row">
                                <td>{{$item->subject}}</td>
                            <td class="text-center"><a href="{{url('/get/profile/view-messege')}}/{{$item->id}}" class="btn btn-primary">view messege</a></td>
                            </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
                <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                    <form action="{{route('user.thread')}}" method="post">
                        <div class="form-group">
                            <label>Subject</label>
                            <input type="text" class="form-control" name="subject" placeholder="Subject of the messege">
                        </div>
                        <div class="form-group">
                            <label>Example textarea</label>
                            <textarea type="text" class="form-control" rows="4" name="message" placeholder="Your Messege"></textarea>
                        </div>
                        <button type="submit" value="submit" class="primary-btn">Send Message</button>
                        <input type="hidden" name="_token" value="{{Session::token()}}">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>