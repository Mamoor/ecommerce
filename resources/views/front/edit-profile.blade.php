<div class="header-profile">
    <h2>Edit Profile</h2>
</div>
<div>
    <div class="col-md-12">
    <form class= "profile" action="{{route('user.profile.update',$data['user']->id)}}" method="post">
                    <!-- COMPONENT END -->
                    <!-- <div class="form-group row">
                        <button type="submit" class="btn btn-primary pull-right" disabled>Submit</button>
                        <button type="reset" class="btn btn-danger">Reset</button>
                    </div> -->
                    <div class="form-group row">
                        <label class="col-sm-2 form-control-label"><b>Name</b></label>
                        <div class="col-sm-10">
                            <p class="form-control-static"><input type="text" class="form-control" value="{{$data['user']->name}}" name="name" placeholder = "Name"></p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 form-control-label"><b>Email</b></label>
                        <div class="col-sm-10">
                            <p class="form-control-static"><input type="email" class="form-control" value="{{$data['user']->email}}" name="email" placeholder = "Email"></p>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 form-control-label"><b>Contact</b></label>
                        <div class="col-sm-10">
                            <p class="form-control-static"><input type="text" class="form-control" value="{{$data['user']->phonenumber}}" name="phonenumber" placeholder = "Contact"></p>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 form-control-label"><b>Address</b></label>
                        <div class="col-sm-10">
                        <textarea class="form-control" rows="2" name="address" placeholder = "Address">{{$data['user']->address}}</textarea>
                        </div>
                    </div>
                    
                    <div class="form-group row pull-right">
                        <div class="col-md-auto">
                            <button type="submit" class="btn btn-primary center-block" style = "background : #1f8612; border : 1px solid #19191a">Update</button>
                        </div>
                    </div>
                    <input type="hidden" name="_token" value="{{Session::token()}}">
                </form>
    </div>
</div>