@extends('layout_user.master')

@section('content')
<!-- Start Banner Area -->
<section class="banner-area organic-breadcrumb">
        <div class="container">
            <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
                <div class="col-first">
                    <h1>Shopping Cart</h1>
                    <nav class="d-flex align-items-center">
                        <a href="index.html">Home<span class="lnr lnr-arrow-right"></span></a>
                        <a href="#">Cart</a>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <!-- End Banner Area -->

    <!--================Cart Area =================-->
    <section class="cart_area">
        <div class="container">
            <div class="cart_inner">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">Product</th>
                                <th scope="col">Price</th>
                                <th scope="col">Quantity</th>
                                <th scope="col">Total</th>
                            </tr>
                        </thead>
                        <tbody>
                        <form class="row" action="{{route('order.user')}}" method="post" novalidate="novalidate">   
                        @foreach($cart as $selected_product)
                            <tr class="product_row">
                                <td>
                                    <div class="media">
                                        <!-- <div class="d-flex">
                                            <img src="{{ asset('storage/'.$selected_product->product->image)}}" alt="">
                                        </div> -->
                                        <div class="media-body">
                                            <p>{{$selected_product->product->name}}</p>
                                            <input type="hidden" name="product_id[]" value="{{$selected_product->product_id}}"
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <h5><span class="price">{{$selected_product->product->price}}</span> /-</h5>
                                </td>
                                <td>
                                    <div class="product_count">
                                        <input type="text" name="qty[]" class="quantity" minlength="0" maxlength="12" value="1" title="Quantity:"
                                            class="input-text qty">
                                        <button onclick="quantity('up',this);"
                                            class="increase items-count" type="button"><i class="lnr lnr-chevron-up"></i></button>
                                        <button onclick="quantity('down',this);"
                                            class="reduced items-count" type="button"><i class="lnr lnr-chevron-down"></i></button>
                                    </div>
                                </td>
                                <td>
                                    <h5><span class="total">{{$selected_product->product->price}}</span> /-</h5>
                                </td>
                            </tr>
                        @endforeach
                            <tr>
                                <td></td>
                                <td></td>
                                <td>
                                    <h5>Subtotal</h5>
                                </td>
                                <td>
                                    <h5><span class="sub-total"></span> /-</h5>
                                </td>
                            </tr>
                            <tr class="out_button_area">
                                <td>

                                </td>
                                <td>

                                </td>
                                <td>

                                </td>
                                <td>
                                    <div class="checkout_btn_inner d-flex align-items-center">
                                        <button type="submit" class="gray_btn">Continue Shopping</button>
                                        <a class="primary-btn" href="{{url('/checkout')}}">Proceed to checkout</a>
                                    </div>
                                </td>
                            </tr>
                             <input type="hidden" name="_token" value="{{Session::token()}}">
                        </form>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!--================End Cart Area =================-->
    
    <script>
        $(document).ready(function(){
            count_subtotal();
        });
        function quantity(check,now){
            var quantity = parseInt($(now).closest('.product_count').find('.quantity').val());
            var price = parseInt($(now).closest('.product_row').find('.price').html());

            if(check == 'up'){
                quantity = quantity + 1;
            }else{
                if(quantity > 1){
                    quantity = quantity - 1;
                }
            }
            $(now).closest('.product_count').find('.quantity').val(quantity);

            var total = quantity * price;

            $(now).closest('.product_row').find('.total').html(total);

            count_subtotal();
        }

        function count_subtotal(){
            var sub_total = 0;
            $('.table .product_row').each(function(){
                var total = parseInt($(this).find('.total').html());

                sub_total = sub_total + total;
            });

            $('.sub-total').html(sub_total);
        }
    </script>
@endsection
