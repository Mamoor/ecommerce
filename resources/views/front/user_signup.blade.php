@extends('layout_user.master')

@section('content')
<!--================Login Box Area =================-->
<section class="login_box_area section_gap">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<div class="login_box_img">
						<img class="img-fluid" src="{{ asset('img/login.jpg') }}" alt="">
						<div class="hover">
							<h4>Already A User?</h4>
							<p>There are advances being made in science and technology everyday, and a good example of this is the</p>
							<a class="primary-btn" href="{{url('/login')}}">Login Here</a>
						</div>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="login_form_inner">
						<h3>Log in to enter</h3>
						<form class="row login_form" action="{{url('/usersignup')}}" method="post">
							<div class="col-md-12 form-group">
								<input type="text" class="form-control" name="name" placeholder="Username" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Username'">
                            </div>
                            <div class="col-md-12 form-group">
								<input type="email" class="form-control" name="email" placeholder="Email Address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email Address'">
							</div>
							<div class="col-md-12 form-group">
								<input type="password" class="form-control" name="password" placeholder="Password" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Password'">
							</div>
							
                            <div class="col-md-12 form-group">
								<input type="text" class="form-control" name="address" placeholder="address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'address'">
							</div>
							<div class="col-md-12 form-group">
                            <input type="text" class="form-control" id="phone" name="phonenumber" placeholder="Phone Number" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Phone Number'">
							</div>
							<div class="col-md-12 form-group">
								<button type="submit" value="submit" class="primary-btn">Register</button>
							</div>
							<input type="hidden" name="_token" value="{{Session::token()}}">
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
    <!--================End Login Box Area =================-->
@endsection

@push('script')
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
   <script><!-- then add javascript code: -->
	$(document).ready(function() {
   	$("#forms).validate({
      rules: {
         firstname: 'required',
         lastname: 'required',
         u_email: {
            required: true,
            email: true,//add an email rule that will ensure the value entered is valid email id.
            maxlength: 255,
         },
      }
   });
});
</script>
@endpush