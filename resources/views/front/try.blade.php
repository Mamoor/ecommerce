@extends('front.master')


@section('content')

<!-- First Form -->
      <<h2>Validation of a form</h2>
      <form id="form" method="post" action="">
         First name:<br>
         <input type="text" name="firstname" value="john">
         <br>
         Last name:<br>
         <input type="text" name="lastname" value="Doe">
         <br>
         Email:<br>
         <input type="email" name="u_email" value="johndoe@gmail.com">
         <br>
         <br><br>
         <input type="submit" value="Submit">
      </form> 
@endsection

@push('script')
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
   <script>
   
}
    $(document).ready(function() {
      $("#form").validate();
   });
   $(document).ready(function() {
   $("#forms).validate({   
      rules: {
         firstname: 'required',
         lastname: 'required',
         u_email: {
            required: true,
            email: true,//add an email rule that will ensure the value entered is valid email id.
            maxlength: 255,
         },
      }
      messages: {
   firstname: 'This field is required',
   lastname: 'This field is required',
   u_email: 'Enter a valid email',
},
  submitHandler: function(form) {
   form.submit();
});

 });
    
   </script>

@endpush