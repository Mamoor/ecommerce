@extends('layout_user.master')
@section('content')
<section class="banner-area organic-breadcrumb">
    <div class="container">
        <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
            <div class="col-first">
                <h1 style="color : #000;">Product Details Page</h1>
                <nav class="d-flex align-items-center">
                    <a href="{{url('/home/product')}}" style="color : #10a633;">Home<span
                            class="lnr lnr-arrow-right"></span>
                    </a>
                </nav>
            </div>
        </div>
    </div>
</section>
<!-- End Banner Area -->
<div class="container">
    <div class="row">
        <div class="col-lg-3">
            <div class="sidebar-profile">
                <div class="head">My Profile</div>
                <ul>
                    <li class="main-nav-list child active"><a href="{{url('/get/profile/my-profile')}}">Profile</a></li>
                    <li class="main-nav-list child"><a href="{{url('/get/profile/edit-profile')}}">Edit Profile</a></li>
                    <li class="main-nav-list child"><a href="{{url('/get/profile/orders')}}">My Orders</a></li>
                    <li class="main-nav-list child"><a href="{{url('/get/profile/support')}}">Support <span><sup style="color : #ba150f">5</sup></span></a></li>
                </ul>
            </div>
        </div>
        <div class="col-lg-9">
            @include('front.'.$partial)
        </div>
    </div>
</div>
<script>
           $('.sidebar-profile .main-nav-list a').on('click', function () {
                $('.sidebar-profile .main-nav-list').find('li.active').removeclass('active');
                $(this).parent('li').addclass('active');
           });
        </script> 
@endsection
