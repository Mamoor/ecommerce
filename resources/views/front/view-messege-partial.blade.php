
@foreach($messages as $msg)
@if($msg->by_whom_type == 'admin')
<div class="incoming_msg">
    <div class="received_msg">
        <div class="received_withd_msg">
            <p>{{$msg->message}}</p>
            <span class="time_date"> {{$msg->created_at}}</span>
        </div>
    </div>
</div>
@else
<div class="outgoing_msg">
    <div class="sent_msg">
        <p>{{$msg->message}}</p>
        <span class="time_date"> {{$msg->created_at}}</span>
    </div>
</div>
@endif
@endforeach