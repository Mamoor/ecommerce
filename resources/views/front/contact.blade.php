@extends('layout_user.master')

@section('content')
<!-- Start Banner Area -->
<section class="banner-area organic-breadcrumb">
		<div class="container">
			<div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
				<div class="col-first">
					<h1>Contact Us</h1>
					<nav class="d-flex align-items-center">
						<a href="{{url('/')}}">Home<span class="lnr lnr-arrow-right"></span></a>
						<a href="{{url('/contact')}}">Contact</a>
					</nav>
				</div>
			</div>
		</div>
	</section>
	<!-- End Banner Area -->

	<!--================Contact Area =================-->
	<section class="contact_area section_gap_bottom">
		<div class="container">
			<div class="row">
				<div class="col-lg-3">
					<div class="contact_info">
						<div class="info_item">
							<i class="lnr lnr-home"></i>
							<h6>Dhaka, Bangladesh</h6>
							<p>Nikunja-2</p>
						</div>
						<div class="info_item">
							<i class="lnr lnr-phone-handset"></i>
							<h6><a href="#">+8801687423058</a></h6>
							<p>Sat to Thu 9am to 9 pm</p>
						</div>
						<div class="info_item">
							<i class="lnr lnr-envelope"></i>
							<h6><a href="#">support@ponno.co</a></h6>
							<p>Send us your query anytime!</p>
						</div>
					</div>
				</div>
				<div class="col-lg-9">
					<form class="row contact_form" action="{{route('contact.with.us')}}" method="post" id="contactForm" novalidate="novalidate">
						<div class="col-md-6">
							<div class="form-group">
								<input type="text" class="form-control" id="name" name="name" placeholder="Enter your name" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your name'">
							</div>
							<div class="form-group">
								<input type="email" class="form-control" id="email" name="email" placeholder="Enter email address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter email address'">
							</div>
							<div class="form-group">
								<input type="text" class="form-control" id="subject" name="subject" placeholder="Enter Subject" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Subject'">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<textarea class="form-control" name="message" id="message" rows="1" placeholder="Enter Message" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Message'"></textarea>
							</div>
						</div>

						<div class="col-md-12 text-right">
						    @if(Auth::guard('usertable')->user()== null)
								<button class="btn btn-rounded logout">
									<a class="logout-button" href="{{url('/login')}}">Log In</a>                    
								</button>
								@else
							<button type="submit" value="submit" class="primary-btn">Send Message</button>
							@endif
						</div>
						<input type="hidden" name="_token" value="{{Session::token()}}">
					</form>
				</div>
			</div>
		</div>
		<div id="map"></div>
	</section>
	<!--================Contact Area =================-->
@endsection

@push('script')
<script>
// Initialize and add the map
function initMap() {
  // The location of Uluru
		 var bangladesh = {lat: 23.6850, lng: 90.3563};
		// The map, centered at Uluru
		var map = new google.maps.Map(
				document.getElementById('map'), {zoom: 7, center: bangladesh});
		// The marker, positioned at Uluru
				// addMarker({lat: 23.822350, lng: 90.365417});

				// addMarker({lat: 23.746466, lng: 90.376015});
				
				// var addMarker(coords){
				// 	var marker = new google.maps.Marker({position: coords, map: map});
				// }
		var marker = new google.maps.Marker(
			{
				position: bangladesh,
				map: map,
		 	});

	var infoWindow = new google.maps.infoWindow({
		content : '<h1>Bangladesh</h1>'
	});
	marker.addListener('click' , function(){
		infoWindow.open(map , marker);
	});
}
    </script>
    <!--Load the API from the specified URL
    * The async attribute allows the browser to render the page while the API loads
    * The key parameter will contain your own API key (which is not needed for this tutorial)
    * The callback parameter executes the initMap() function
    -->
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAziTDBXVKGG2WfLDEY-iQODQf9Ez71gKU&callback=initMap">
    </script>
@endpush