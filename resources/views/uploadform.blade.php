<!DOCTYPE html>
<html>
<head lang="en">
	
<title>Image Upload</title>
</head>

<body>

<form action="{{route('showform')}}" method="post" enctype="multipart/form-data">

                <div class="form-group row">
                    <label class="col-sm-2 form-control-label">Image</label>
                        <div class="col-sm-10 input-group input-file" name="Fichier1">
                            <input type="file" class="form-control" placeholder='Choose a file...' name="image" />          
                            <span class="input-group-btn">
                                <button class="btn btn-default btn-choose" type="button">Choose</button>
                            </span>
                        </div>

                        <div class="form-group row">
                        <div class="col-md-auto">
                            <button type="submit" class="btn btn-primary center-block">Submit</button>
                        </div>
                    </div>
                    <input type="hidden" name="_token" value="{{Session::token()}}">


</form>

   
</body>
</html>