<?php

namespace App;
use Auth;
use Illuminate\Database\Eloquent\Model;
class Usertable extends model 
{
    protected $table = "usertables";
    //protected $guard = "usertable";

    public function carts()
    {
        return $this->belongsTo('App\Cart');
    }
    public function product(){
        return $this->hasMany('App\Orderproduct','id','user_id');
    }
}
