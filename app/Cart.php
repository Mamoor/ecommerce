<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
        // public function catr()
        // {
        //     return $this->hasMany('App/Pltable');
        // }

        public function user()
        {
            return $this->belongsTo('App\Usertable','user_id','id');
        }
        
        public function product()
        {
            return $this->belongsTo('App\Pltable','product_id','id');
        }
}
