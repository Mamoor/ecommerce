<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orderproduct extends Model
{
    public function order_product_id(){
        return $this->belongsTo('App\Usertable','user_id','id');
    }

    public function order_id(){
        return $this->belongsTo('App\Order','order_id','id');
    }

    public function products_id(){
        return $this->belongsTo('App\Pltable','product_id','id');
    }
}
