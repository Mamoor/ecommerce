<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pltable extends Model
{
      protected $table="pltables";

      public function product_in_cart()
      {
         return $this->belongsTo('App\Cart','id','product_id');

      }
     public function categoryId()
     {
           return $this->belongsTo('App\Category','category','id');
     }
}
