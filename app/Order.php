<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function order_table(){
        return $this->belongsTo('App\Cart','client_id','user_id');
    }
   
    public function order_of_user(){
        return $this->belongsTo('App\Usertable','client_id','id');
    }
    public function order_product(){
        return $this->belongsTo('App\Orderproduct','id','order_id');
    }

    

}
