<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Thread extends Model
{
    public function thread(){
        return $this->belongsTo('App\Usertable','user_id','id');
    }

    public function messages(){
        return $this->hasMany('App\Message','thread_id');
    }
}
