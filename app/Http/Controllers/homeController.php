<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pltable;
use App\Cart;

use Illuminate\Support\Facades\Auth;


class HomeController extends Controller{

    public function get_product(){
        //dd(Auth::guard('usertable')->user());
      $Pltable=Pltable::orderBy('created_at','desc')->get();
      $data=['tables'=>$Pltable];
      return view('front.home',compact('data'));
    }

    public function add_to_cart($id){
        $logged = Auth::guard('usertable')->user();
        $check_product = Cart::where('user_id', $logged->id)->where('product_id',$id)->first();
        if($check_product){
            dd('Alredy added to cart');
        }else{
            $Pltable=Pltable::find($id);
            if($logged){
                $cart=new Cart();
                $cart->product_id=$Pltable->id;
                $cart->user_id=$logged->id;
                $cart->save();
                return redirect()->back();
            }else{
                return view('front.user_signin');
            }
        }
     }
  
    public function view_cart(){
        $logged = Auth::guard('usertable')->user();
        if($logged == null){
            return view('front.user_signin');
        }else{
            $cart=Cart::with('product')->where('user_id',$logged->id)->orderBy('created_at','desc')->get();
            return view('front.cart', compact('cart'));
            
        }
    }

}
