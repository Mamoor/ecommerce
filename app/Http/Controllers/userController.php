<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Redirect;
use Auth;
use App\Usertable;
use Illuminate\Http\Request;
use Illuminate\Supports\Facades;
use App\Commentstable;
use App\Order;
use App\Orderproduct;
use App\Cart;
use App\Pltable;
use App\Thread;

class userController extends Controller{
 
 public function userSignUp(Request $request){
    $this->validate($request,[
      'email'=>'required|unique:users',
      'name'=>'required|max:120',
      'phonenumber'=>'required',
      'password'=>'required|min:4'
      ]);
    $name=$request['name'];
    $email=$request['email'];
    $address=$request['address'];
    $phonenumber=$request['phonenumber'];
    $password=bcrypt($request['password']);

    $Usertable=new Usertable();
    $Usertable->name=$name;
    $Usertable->email=$email;
    $Usertable->address=$address;
    $Usertable->password=$password;
    $Usertable->phonenumber=$phonenumber;

    $Usertable->save();
    return view('front.user_signin');

 }

  public function userSignIn(Request $request){
    if (Auth::guard('usertable')->attempt(['email'=> $request['email'],'password'=> $request['password']], true)) {
      $logged = Auth::guard('usertable')->user();
     // $id=$logged->id;
      $user=Usertable::find($logged->id);
      return redirect('/get/profile/my-profile');
    }else{
      return Redirect::back()->with('error','Wrong Email/Password!');
    }

  }
   
    public function getProfile($partial, $id = null){
      $logged = Auth::guard('usertable')->user();
      $data = array();
      if($partial == 'my-profile'){
        $user=Usertable::find($logged->id);
        $data = array(
          'user' => $user
        );
      }elseif($partial == 'edit-profile'){
        $user=Usertable::find($logged->id);
       $data = array(
          'user' => $user
        );  
       // dd($data);
      }elseif($partial == 'support'){
        $treads =Thread::where('user_id',$logged->id)->get();
        $data = array(
          'threads' => $treads
        );  
      }elseif($partial == 'view-messege'){
        $thread = Thread::with(['messages'])->where('user_id',$logged->id)->find($id);
        $data = array(
          'thread' => $thread
        );
      }

      return view('front.profile',compact('data','partial'));
    }

    public function update_profile(Request $request,$id){
      $user=Usertable::find($id);
      //dd($user);
      $user->name=$request['name'];
      $user->email=$request['email'];
      $user->address=$request['address'];
      $user->phonenumber=$request['phonenumber'];
      $user->save();
      return redirect()->back();
    }

    public function userLogOut()
      {
        Auth::guard('usertable')->logout();
        return view('front.user_signin');

      }

    public function usercomments(Request $request){
      $comment=new Commentstable();
      $comment->name=$request['name'];
      $comment->email=$request['email'];
      $comment->subject=$request['subject'];
      $comment->message=$request['message'];
      $comment->save();
      return redirect()->back();
    }  
    
    public function order_product(Request $request){
      //dd($request);
      $logged = Auth::guard('usertable')->user();
      $order=new Order();
      $order->client_id=$logged->id;
      $order->email=$logged->email;
      $check = $order->save();

      $total = 0;

      if($check == true){
        $products = $request->product_id;
       $qty = $request->qty;

        foreach($products as $key => $row){
            $add_product = new Orderproduct;
            $add_product->product_id = $row;
            $add_product->quantity = $qty[$key];
            $add_product->user_id=$logged->id;
            $add_product->order_id = $order->id;
            $add_product->save(); 

            $product = Pltable::find($row);

            $total = $total + ($product->price * $qty[$key]);
        }

        $order->total = $total;
        $order->update();
        Cart::where('user_id', $logged->id)->delete();
      }

      return redirect()->back();
  }
       



}