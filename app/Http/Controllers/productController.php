<?php
namespace App\Http\Controllers;

use App\Pltable;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Auth;
use Illuminate\Support\Facades\Storage;
use App\Category;
use App\Cart;
use Illuminate\Pagination\Paginator;

class productController extends Controller
{
	public function productUploadForm(){
		$categories = Category::all();
		return view('admin.product_form', compact('categories'));
	}
    public function productInput(Request $request){
        $this->validate($request, [    
    'name'=>'required|max:100',
    'category'=>'required',
    'price'=>'required'

  ]);

        /* $name=$request['name'];
         $category=$request['category'];
         $description=$request['description'];
         $price=$request['price'];
         $quantity=$request['quantity'];
         $image=$request['image'];*/

        /*
             $image=Storage::disk('public')->put('product', $request->file('image'));


        /* $file=$request->file('image');
          $file_name=$request['name'].'-'.$Pltable->id.'.jpg';

          if($file){

            Storage::disk('local')->put($file_name,File::get($file));
          }
*/
    
        if ($request->has('image')) {
            $Pltable=new Pltable();
            $Pltable->name=$request['name'];
            $Pltable->category=$request['category'];
            $Pltable->description=$request['description'];
            $Pltable->quantity=$request['quantity'];
            $Pltable->price=$request['price'];
            $Pltable->image =Storage::disk('public')->put('folder', $request->file('image'));
            $Pltable->save();

            return redirect()->route('getdata');
        }
    }
    //try to store image
    /* public function storeImage(Request $request)
     {
         $file=$request->file('image');
         $file_name=$request['name'].'-'.$Pltable->id.'.jpg';

         if($file){

           Storage::disk('local')->put($file_name,File::get($file));
         }

       return redirect()->back();

     }*/
   
    //try to store image
    /* public function getImage($file_name)
     {

        $file=Storage::disk('local')->get($file_name);
        return new Response($file,100);

     }*/

    public function getData(){
        $Pltable=Pltable::with('categoryId')->orderBy('created_at', 'desc')->take(5)->get();
        $data=['tables'=> $Pltable];
        return view('admin.product', compact('data'));
    }
  

    public function getCategory(){
        $category=Category::orderBy('created_at', 'desc')->take(5)->get();
        $data=['tables'=> $category];
        return view('admin.category', compact('data'));
    }

    public function getDelete($id){
        $Pltable=Pltable::find($id);
        if (!is_null($Pltable)) {
            $Pltable->delete();
        }
        return redirect()->back();
    }

    public function product_edit($id){
        $Pltable=Pltable::find($id);
              
        return view('admin.edit_product', compact("Pltable"));
    }
            
    public function update(Request $request, $id){
        $this->validate($request, [
        
        'name'=>'required|max:100',
        'category'=>'required',
        'price'=>'required'
  
      ]);
       
        $Pltable=Pltable::find($id);
        $Pltable->name=$request['name'];
        $Pltable->category=$request['category'];
        $Pltable->description=$request['description'];
        $Pltable->quantity=$request['quantity'];
        $Pltable->price=$request['price'];
        $Pltable->image =Storage::disk('public')->put('folder', $request->file('image'));
        $Pltable->save();
    
        return redirect()->back();
    }
  
    public function createCategory(Request $request){
        $category=new Category();
        $category->name=$request['name'];
        $category->save();

        return redirect()->back();
    }

    public function categoryList($id){
        $category=Category::orderBy('created_at', 'desc')->get();
        return view('front.product_list', compact('category'));
    }

    public function productList($cat, $sort, $show){
        $logged = Auth::guard('usertable')->user();

        if($logged){
            $product_selected = Cart::where('user_id', $logged->id)->pluck('product_id')->toArray();
        }else{
            $product_selected = array();
        }

        if($cat == 0){
            $products = Pltable::with('categoryId')->orderBy('created_at',$sort)->paginate($show);
        }else{
            $products = Pltable::with('categoryId')->where('category', $cat)->orderBy('created_at',$sort)->paginate($show);
        }
        
        return view('front.product_list_partial', compact('products','product_selected'));
    }
     
    public function categoryBrowse($id){
        $catbrowse=Category::find($id)->with('category')->get();
        return view('front.product_list',compact('catbrowse'));
    }

    public function categoryDelete($id){
        $category=Category::find($id);
        if (!is_null($category)) {
            $category->delete();
        }
        return redirect()->back();
    }

    public function category_edit($id){
        $category=Category::find($id);
       return view('admin.edit_category', compact('category'));
    }
    public function updateCategory(Request $request, $id){
        $this->validate($request, [
       'name'=>'required|max:100'
   ]);
        $category=Category::find($id);
        $category->name=$request['name'];
        $category->save();
    
        return redirect()->back();
    }
    public function view_product($id){
       $pltable=Pltable::find($id);
      // dd($pltable);
       return view('front.Product_detail',compact('pltable'));
    }


}
