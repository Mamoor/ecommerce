<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Order;
use App\Cart;
use App\Orderproduct;

class orderController extends Controller
{
    public function getOrder(){
       // $order=Order::with('order_of_user','order_product')->orderBy('created_at','desc')->get();
        $order=Order::with('order_of_user','order_product')->orderBy('created_at','desc')->get();
        $data=['tables'=>$order];
       // $orderproduct=Orderproduct::with('order_product_id')->orderBy('created_at','desc')->get();
        return view('admin.order',compact('data'));
    }

    public function getOrderDelete($id){
     $order=Order::find($id);
     
        if(!is_null($order)){
            $order->delete();
        }
     return redirect()->back();
    }

   public function editOrder($id){
       $order=Order::find($id);
       return view('admin.edit_order',compact("order"));
   }

   public function updateOrder(Request $request, $id){
    $this->validate($request, [
   'name'=>'required|max:100'
]);
    $category=Category::find($id);
    $category->name=$request['name'];
    $category->save();

    return redirect()->back();
}

    public function order_product($id){
        $orderproduct=Orderproduct::where('order_id',$id)->with('products_id')->orderBy('created_at','desc')->get();
        //dd($orderproduct);
        return view('admin.invoice',compact('orderproduct'));
    }


 
}
