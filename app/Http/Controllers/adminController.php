<?php
namespace App\Http\Controllers;

use App\admin;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Supports\Facades;
use Illuminate\Http\Response;
use Illuminate\Supports\Facades\File;
use Illuminate\Supports\Facades\Storage;
use App\Thread;

class adminController extends Controller
{
 
 public function adminSignUp(Request $request){
     $this->validate($request,[
     'email'=>'required|unique:admins',
     'name'=>'required|max:120',
     'password'=>'required|confirmed|min:4'
     ]);
    $name=$request['name'];
    $email=$request['email'];
    $password=bcrypt($request['password']);

    $admin = new admin();
    $admin->name=$name;
    $admin->email=$email;
    $admin->password=$password;

    $admin->save();

    return view('admin.dashboard');
}

  public function adminSignIn(Request $request){
    if(Auth::guard('admin')->attempt(['email'=> $request['email'], 'password'=> $request['password']])){
    return view('admin.dashboard');
   }
  return redirect()->back();
  }


   public function getAdmins(){
     $admin=admin::orderBy('created_at','desc')->take(5)->get();
     $data=['tables'=>$admin];
    return view('admin.admins',compact('data'));
  }

  public function getLogOut()
  {
    Auth::guard('admin')->logout();
    return redirect()->back();
  }
  
  public function changeStatus(Request $request){
  $id = $request->id;
  $status = (int) $request->status;
  $admin = admin::find($id);

    if(!empty($admin)){
    $admin->status = $status;
    $admin->save();
    return response()->json(["success" => true, "msg" => "Status changed!"], 200);
    } 

    else {
      return response()->json(["success" => false, "msg" => "No admin found with this id!"], 200);
        }
  }

  public function getDeleteAdmin($id){
  $admin=admin::find($id);
  if(!is_null($admin)){
    $admin->delete();
  }
  return redirect()->back();
 }

  public function admin_edit($id){ 
   $admin=admin::find($id);
  return view('admin.edit_admin', compact("admin"));
  } 

  public function update_admin(Request $request,$id){
    $this->validate($request,[
    'name'=>'required|max:100',
    'email'=>'required|'
    ]);

    $name=$request['name'];
    $email=$request['email'];

    $admin=admin::find($id);
    $admin->name=$name;
    $admin->email=$email;
    $admin->save();

  return redirect()->back();
  }
  public function message(){
    $threads=Thread::with('thread')->orderBy('created_at','asc')->get();
   // dd($threads);
    return view('admin.messege',compact('threads'));
  }





}