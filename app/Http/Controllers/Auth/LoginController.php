<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
//use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    //use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        
        return view('admin.auth.login');
    }

    public function login(\Illuminate\Http\Request $request)
    {
        $this->validate($request,[
            'email'=>'required|exists:admins',
            'password'=>'required|min:4'
        ]);

        if(Auth::guard('admin')->attempt(['email'=> $request['email'], 'password'=> $request['password']], true))
        {
           return redirect('admin/dashboard');
        }
        return redirect()->back();
    }
}
