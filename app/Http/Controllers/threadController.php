<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Thread;
use Illuminate\Support\Facades\Auth;
use App\Message;

  class threadController extends Controller{

  public function thread_function(Request $request){
    // dd($request);
    $logged = Auth::guard('usertable')->user();

    $thread= new Thread();
    $thread->subject=$request['subject'];
    $thread->user_id=$logged->id;
    $thread->save();

    $this->msg_save($request, $thread->id);
  return redirect()->back();

  }

  public function msg_store(Request $request, $thread_id){
    $logged = Auth::guard('usertable')->user();

    $thread= Thread::where('user_id', $logged->id)->find($thread_id);

  if(!empty($thread)){
    $this->msg_save($request, $thread_id);
  }
  }

  public function thread_load($thread_id){
    $logged = Auth::guard('usertable')->user();
    $messages= Message::where('thread_id', $thread_id)->orderBy('id', 'asc')->get();
  return view('front.view-messege-partial',compact('messages'));
  }

    // public function admin_thread_load($thread_id){
    //   $logged = Auth::guard('usertable')->user();
    //   $messages= Message::where('thread_id', $thread_id)->orderBy('id', 'asc')->get();
    //   return view('front.view-messege-partial',compact('messages'));
  // }
  private function msg_save(Request $request, $thread_id){
    $logged = Auth::guard('usertable')->user();

    $message=new Message();
    $message->thread_id= $thread_id;
    $message->message=$request['message'];
    $message->by_whom_type= 'user';
    $message->by_whom_id= $logged->id;
    $message->seen_status= 0;
    $message->save();
  }
  
  public function Delete_thread($id){
    $messages=Message::where('thread_id',$id)->delete();
    $thread=Thread::find($id);
    $thread->delete();
  return redirect()->back();
  }
    


}
