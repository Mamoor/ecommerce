<?php

namespace App\Http\Controllers;
use App\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;


class imageController extends Controller
{

   public function showUploadForm(){

   return view('uploadform');
   }
    

   public function imageController(Request $request){
   if($request->has('image')){
      $image = new Image;
      $image->name = Storage::disk('public')->put('product', $request->file('image'));
      $image->size = $request->file('image')->getClientSize();
      $image->save();

   return redirect('show');
     }
  }

   public function showImage(){
    $picture=Image::all();
   return view('show',compact('picture'));
   }

   public function get_session_value(Request $request){
   $request->session()->put('user',$request->input('name'));
      //echo $request->session()->get('user');
      return view('front.sessionvalue')->with('data',$request->session()->get('user'));
   }


}
