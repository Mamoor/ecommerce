<?php
use App\Http\Controllers\userController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/admin','Auth\LoginController@showLoginForm')->name('admin.login');
Route::post('/admin-login',[
    'uses'=>'Auth\LoginController@login',
    'as'=>'admin-login'
]);
Route::middleware('auth:admin')->group(function(){
    Route::get('/admin/dashboard', 'dashboardController@totalProduct');

    Route::get('/admin/product/p_form','productController@productUploadForm');

    Route::post('/pltable',[
    'uses'=>'productController@productInput',
    'as'=>'pltable'
    ]);

    Route::get('/getimage/{file_name}',[
    'uses'=>'productController@getImage',
    'as'=>'getimage'
    ]);

    Route::get('admin/product',[
    'uses'=>'productController@getData',
    'as'=>'getdata'
    ]);

    Route::get('/getdelete/{id}',[
        'uses'=>'productController@getDelete',
        'as'=>'getdelete'
    ]);

    Route::get('/category/delete/{id}',[
        'uses'=>'productController@categoryDelete',
        'as'=>'category.delete'
    ]);

    Route::get('/admin/order','orderController@getOrder')->name('getorder');
    Route::get('/admin/order/product','orderController@order_product')->name('order');
    Route::get('/admin/order/{id}','orderController@getOrderDelete')->name('orderdelete');
    Route::get('/admin/order/edit_order/{id}','orderController@editorder')->name('edit.order');


    Route::get('admin/category',[
    'uses'=>'productController@getCategory',
    'as'=>'getcategory'
    ]);

    Route::get('admin/category/create_category', function () {
        return view('admin.category_form');
    });
    Route::post('/category/create_category','productController@createCategory')->name('creat.category');


    Route::get('admin/admins/create_admin', function () {
        return view('admin.admin_form');
    });

    //for test
    Route::get('/admin/invoice/{id}','orderController@order_product');

    //test routes

    Route::get('/admin/product/edit/{id}',[
        'uses'=>'productController@product_edit',
        'as'=>'productedit'
    ]);
    Route::post('/update/{id}',[
        'uses'=>'productController@update',
        'as'=>'update.product'
    ]);

    Route::get('/admin/category/edit_category/{id}','productController@category_edit')->name('category.edit');
    Route::post('/admin/category/update/{id}','productController@updateCategory')->name('category.update');

    Route::get('admin/admins/edit_admin/{id}','adminController@admin_edit')->name('adminedit');

    Route::get('/admin/order/edit_order', function () {
        return view('admin.edit_order');
    });

    Route::get('upload','imageController@showUploadForm')->name('showform');

    Route::post('upload','imageController@imageController');

    Route::get('show','imageController@showImage')->name('showImage');

    //Route::get('/admin','userController@totalUser')->name('count.user.number');    
    Route::get('admin/admins/{id}','adminController@getDeleteAdmin')->name('admindelete');

    Route::post('admin/admins/edit_admin/{id}','adminController@update_admin')->name('update.admin');
    Route::get('/admin/admins',[
    'uses'=>'adminController@getAdmins',
    'as'=>'getadmins'
    ]);
    
    Route::post('/admin/admins/change_status', 'adminController@changeStatus')->name('admin.change.status');

    Route::post('/logout',[
        'uses'=>'adminController@getLogOut',
        'as'=>'logout'
    ]);
});

Route::post('/adminsignup',[
'uses'=>'adminController@adminSignUp',
'as'=>'signup'
]);

Route::get('/signup', function () {
    return view('admin.signup');
});

Route::get('/reset', function () {
    return view('admin.reset');
});

Route::get('/admin/support', function () {
    return view('admin.messege');
});

Route::get('/admin/support/view/user/message', function () {
    return view('admin.view-messege');
});
Route::get('/admin/support/view','adminController@message')->name('admin.viwe.message');


Route::get('/signin', function () {
    return view('admin.signin');
});

Route::post('/signin',[
'uses'=>'Auth\LoginController@login',
'as'=>'signin'
]);



//frontEnd Routes
Route::get('home/product','homeController@get_product')->name('home.product');
Route::get('home/product/{id}','homeController@add_to_cart')->name('cart.product');

Route::get('/login', function () {
    return view('front.user_signin');
});
 Route::get('/category/browse/{id}','productController@categoryBrowse')->name('category.browse');
 Route::get('/products/{id}','productController@categoryList')->name('front.product.category.browse');
 Route::get('/products/partial/{cat}/{sort}/{show}','productController@productList')->name('front.product.partial');
 Route::get('/view/product/{id}','productController@view_product')->name('view.product');

Route::get('/cart','homeController@view_cart')->name('cart.view');

Route::get('/checkout', function () {
    return view('front.checkout');
}); 

Route::get('/details', function () {
    return view('front.product_detail');
}); 

Route::post('/usersignin',[
    'uses'=>'userController@userSignIn',
    'as'=>'userlogin'
]);

Route::get('/register', function () {
    return view('front.user_signup');
});

Route::post('/usersignup',[
    'uses'=>'userController@userSignUp',
    'as'=>'userregister'
    ]);

Route::get('/userlogout',[
    'uses'=>'userController@userLogOut',
    'as'=>'userlogout'
    ]);
 
Route::get('/contact', function () {
    return view('front.contact');
});

Route::post('/contact/with/us','userController@usercomments')->name('contact.with.us');
Route::post('/order','userController@order_product')->name('order.user');

Route::get('/get/profile/{partial}','userController@getProfile')->name('get.profile');
Route::get('/get/profile/{partial}/{id}','userController@getProfile')->name('get.profile');
Route::post('/get/profile/thread','threadController@thread_function')->name('user.thread');
Route::post('/get/profile/thread/store/{thread}','threadController@msg_store')->name('user.thread.msg.store');
Route::post('/get/profile/update/{id}','userController@update_profile')->name('user.profile.update');
Route::get('/threads/partial/{thread_id}','threadController@thread_load')->name('user.thread.load');
Route::get('/admin/support/delete/message/{id}','threadController@Delete_thread')->name('delete.user.thread');


Route::get('/session/learning', function () {
    return view('front.session');
});
Route::post('/get/session/value','imageController@get_session_value')->name('get.session.value');


//cache clear
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('config:clear');
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('config:cache');
    return 'DONE'; 
});
