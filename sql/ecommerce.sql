-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 30, 2019 at 02:29 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ecommerce`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `created_at`, `updated_at`, `name`, `email`, `password`, `status`, `remember_token`) VALUES
(1, '2019-03-31 04:50:32', '2019-04-18 00:21:30', 'ashik', 'ashik@test.com', '$2y$10$75Jio/i1ec8R8ojSUKs51OQipoO2ro5zI2D5WkMn0BIDASdwW2NT.', 0, NULL),
(2, '2019-03-31 04:55:23', '2019-04-18 00:21:28', 'sourav', 'souravsingha1304021@gmail.com', '$2y$10$FVzM9q3Qe7fv8aa7RCsSA.Uspm.nd0ZG86fnYR/BgG8x/FtDQmoni', 1, 'e4D739tFQe7tsfMRdOyYlcL5giximzECSXatKcl1vu9q5rKW5WY1HHsPee8E');

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE `carts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `created_at`, `updated_at`, `name`) VALUES
(1, '2019-04-18 04:41:40', '2019-04-25 02:09:29', 'kid'),
(3, '2019-04-18 05:30:55', '2019-04-18 05:30:55', 'men'),
(4, '2019-04-18 05:31:20', '2019-04-18 05:31:20', 'women');

-- --------------------------------------------------------

--
-- Table structure for table `commentstables`
--

CREATE TABLE `commentstables` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `commentstables`
--

INSERT INTO `commentstables` (`id`, `created_at`, `updated_at`, `name`, `email`, `subject`, `message`) VALUES
(1, '2019-04-23 06:17:16', '2019-04-23 06:17:16', 'sourav singha', 'souravsingha96@gmail.com', 'test', 'test'),
(2, '2019-04-23 06:28:57', '2019-04-23 06:28:57', 'zaheen', 'zaheen.mahdee.@gmail.com', 'test', 'it\'s for testing');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `name`, `size`, `created_at`, `updated_at`) VALUES
(11, 'product/4YBbQlYiTi65u9cNYtKOCjgacF32zcn2BFjNAekL.jpeg', '52023', '2019-04-08 01:11:20', '2019-04-08 01:11:20');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `thread_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `by_whom_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `by_whom_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seen_status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `thread_id`, `message`, `by_whom_type`, `by_whom_id`, `seen_status`, `created_at`, `updated_at`) VALUES
(1, '1', 'test', 'user', '2', 0, '2019-04-30 05:14:31', '2019-04-30 05:14:31'),
(2, '2', 'test2', 'user', '2', 0, '2019-04-30 05:22:17', '2019-04-30 05:22:17');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2019_03_28_065523_create_users_table', 1),
(2, '2019_03_28_065618_create_admins_table', 1),
(3, '2019_04_02_100728_create_pltables_table', 2),
(4, '2019_04_06_072442_create_images_table', 2),
(5, '2019_04_07_074304_create_orders_table', 2),
(6, '2019_04_07_075230_create_oplists_table', 2),
(7, '2019_04_11_074542_create_usertables_table', 2),
(8, '2019_04_16_074308_create_carts_table', 2),
(9, '2019_04_18_064059_create_orderproducts_table', 2),
(10, '2019_04_18_100022_create_categories_table', 3),
(11, '2019_04_23_103028_create_commentstables_table', 4),
(12, '2019_04_29_090602_create_threads_table', 5),
(15, '2019_04_29_092645_create_messages_table', 6);

-- --------------------------------------------------------

--
-- Table structure for table `orderproducts`
--

CREATE TABLE `orderproducts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `product_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orderproducts`
--

INSERT INTO `orderproducts` (`id`, `created_at`, `updated_at`, `product_id`, `quantity`, `user_id`, `order_id`) VALUES
(1, '2019-04-24 06:24:27', '2019-04-24 06:24:27', '17', '1', 2, NULL),
(2, '2019-04-24 06:25:14', '2019-04-24 06:25:14', '15', '4', 2, NULL),
(3, '2019-04-24 06:26:18', '2019-04-24 06:26:18', '17', '2', 2, NULL),
(4, '2019-04-24 07:35:35', '2019-04-24 07:35:35', '15', '2', 6, NULL),
(5, '2019-04-24 07:35:35', '2019-04-24 07:35:35', '18', '1', 6, NULL),
(27, '2019-04-28 00:51:38', '2019-04-28 00:51:38', '14', '1', 2, 39),
(28, '2019-04-28 00:51:38', '2019-04-28 00:51:38', '12', '1', 2, 39),
(29, '2019-04-28 03:37:09', '2019-04-28 03:37:09', '11', '1', 2, 40),
(30, '2019-04-28 03:37:09', '2019-04-28 03:37:09', '15', '1', 2, 40),
(31, '2019-04-29 03:20:10', '2019-04-29 03:20:10', '16', '1', 2, 41);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `client_id` int(255) NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `created_at`, `updated_at`, `client_id`, `email`, `total`) VALUES
(5, '2019-04-28 00:10:34', '2019-04-28 00:10:35', 2, 'souravsingha96@gmail.com', 5600),
(39, '2019-04-28 00:51:38', '2019-04-28 00:51:38', 2, 'souravsingha96@gmail.com', 2500),
(40, '2019-04-28 03:37:09', '2019-04-28 03:37:09', 2, 'souravsingha96@gmail.com', 2720),
(41, '2019-04-29 03:20:10', '2019-04-29 03:20:10', 2, 'souravsingha96@gmail.com', 1800);

-- --------------------------------------------------------

--
-- Table structure for table `pltables`
--

CREATE TABLE `pltables` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` int(255) NOT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pltables`
--

INSERT INTO `pltables` (`id`, `created_at`, `updated_at`, `name`, `category`, `price`, `quantity`, `description`, `image`) VALUES
(4, '2019-04-03 03:49:12', '2019-04-03 03:49:12', 'ashik', 1, '234', '1', 'huhuhuhu', 'media-share-0-02-03-ab78b215a8d9e8dd567bb8610810f7aad1bfd1c6ab20c28decc958eb4c3b11d3-d08e3d3e-c9d5-46ae-aa32-49cc52472089.jpg'),
(9, '2019-04-15 22:38:59', '2019-04-15 22:38:59', 't-shirt', 1, '120', '1', 'jsihue', 'folder/WBXz9JK9gPqEP1t9nMNMECRozZC8jrEvRHQnrThC.jpeg'),
(10, '2019-04-18 06:41:38', '2019-04-18 06:41:38', 't-shirt', 1, '300', '1', 'T-shirt for kids', 'folder/WCaIBLiD2qzR8ZzrFZHDqgpfAvDYByECEUAO4qCR.jpeg'),
(11, '2019-04-18 06:52:16', '2019-04-18 06:52:16', 't-shirt', 3, '220', '1', 'men t-shirt', 'folder/NPhH47vVX3UpiZAedDYCjny4DD8Oh8dwIRr2i368.jpeg'),
(12, '2019-04-18 06:53:48', '2019-04-18 06:53:48', 't-shirt', 4, '300', '1', 'women t-shirt', 'folder/JPfodpudOjyYUiyrqgVMPaWtPss7eTUoIthamrGP.jpeg'),
(13, '2019-04-21 06:08:53', '2019-04-21 06:08:53', 'shirt', 3, '2000', '1', 'men shirt', 'folder/6uW2k7uFBuLLDfAj7ZiufNEoG18K1XjzKOP5mHVI.jpeg'),
(14, '2019-04-21 06:09:29', '2019-04-21 06:09:29', 'shirt', 3, '2200', '1', 'men shirt', 'folder/vBccMvzaDImqyzCopQ6ZXRUlB4szF3uFSbVEeIom.jpeg'),
(15, '2019-04-21 06:10:57', '2019-04-21 06:10:57', 'shirt', 3, '2500', '1', 'men shirt', 'folder/A8B9VXVoOKeRbYSUnNyfOskcIfLqv7HB5GGBqyp4.png'),
(16, '2019-04-21 06:11:50', '2019-04-21 06:11:50', 'shirt', 4, '1800', '1', 'women shirt', 'folder/GUz4C0LaTcb571SSJdQnhXfH4uPhLtxmaqxcfeO4.jpeg'),
(17, '2019-04-21 06:12:23', '2019-04-21 06:12:23', 'shirt', 4, '2000', '1', 'women shirt', 'folder/0MFhp0Fjfvm2GkHoIogUNh9bEB3YXxNFPnzbgzuw.jpeg'),
(18, '2019-04-21 06:22:31', '2019-04-21 06:22:31', 't-shirt', 4, '1990', '1', 'women t-shirt', 'folder/V8i7j3bzlc6YUpMPxE7IfOD5yuCoH5bxhUfJRW1E.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `threads`
--

CREATE TABLE `threads` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `threads`
--

INSERT INTO `threads` (`id`, `subject`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'help', '2', '2019-04-30 05:14:31', '2019-04-30 05:14:31'),
(2, 'help', '2', '2019-04-30 05:22:16', '2019-04-30 05:22:16');

-- --------------------------------------------------------

--
-- Table structure for table `usertables`
--

CREATE TABLE `usertables` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phonenumber` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `usertables`
--

INSERT INTO `usertables` (`id`, `created_at`, `updated_at`, `name`, `email`, `address`, `phonenumber`, `password`, `remember_token`) VALUES
(2, '2019-04-16 04:12:37', '2019-04-16 04:12:37', 'sourav singha klinton', 'souravsingha96@gmail.com', 'narail', '01910290103', '$2y$10$GHSxVFtDQ3wYQiKCzCWua.2ulPAw4jFnBxNYwX109iHhlJcCCut0m', 'APs5Bnt37KqrGvIUEmA1QoK1aZqfdkwfz5Pw6wCE8MNnjCiPlv9kVBBjExG9'),
(4, '2019-04-21 00:30:05', '2019-04-21 00:30:05', 'zaheen', 'zaheen.mahdee.91@gmail.com', 'dhaka', '12345', '$2y$10$S1pX/2rk6BM7Ay4NAEr90ek57DsC646nEhOtUIaa0xsFBcGj5LM6a', NULL),
(6, '2019-04-21 00:31:36', '2019-04-21 00:31:36', 'zaheen', 'zaheen.mahdee.@gmail.com', 'dhaka', '12345', '$2y$10$j.URCoth8TB1iyaGvlS8D..6enaFU/ylE0C7RmtTr.hvfiUAnADLi', NULL),
(9, '2019-04-21 02:33:09', '2019-04-21 02:33:09', 'klinton Singha', 'souravsingha@gmail.com', 'dhaka', '01910290103', '$2y$10$o3vHsuoRwq4J08PxRTjjFegWCBOiEt//sjjC09BOsCE53BXkSxNQm', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `commentstables`
--
ALTER TABLE `commentstables`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orderproducts`
--
ALTER TABLE `orderproducts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pltables`
--
ALTER TABLE `pltables`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `threads`
--
ALTER TABLE `threads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usertables`
--
ALTER TABLE `usertables`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `carts`
--
ALTER TABLE `carts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `commentstables`
--
ALTER TABLE `commentstables`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `orderproducts`
--
ALTER TABLE `orderproducts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `pltables`
--
ALTER TABLE `pltables`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `threads`
--
ALTER TABLE `threads`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `usertables`
--
ALTER TABLE `usertables`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
